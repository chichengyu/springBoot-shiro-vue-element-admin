import types from './types.js'
import route from '../router/route'
import {routes} from '../router/routes'
import {handleRoutesMap,routesMap} from '@/router/route/router_map'
const {components} = require('@/settings.js');

function hasAccess(routerList,parentPath) {
    let newRouterList = [];
    if (!routerList || !routerList.length > 0){
        //console.log('%c ! Menu Data Array Error ','background:#67C23A;color:#fff',routerList);
        return newRouterList;
    }
    routerList.map(el => {
        if (!el.path && !el.name && !el.children){
            console.log('%c ! Menu Data Error，path OR name is Not Null OR Empty ','background:#000;color:#bada55',el);
            return false;
        }
        if (el.children){
            let componentPath = el.path;
            if (componentPath){
                if (parentPath){
                    componentPath = (parentPath.startsWith("/")?parentPath.substr(1):parentPath)+componentPath;
                    /*if (componentPath.includes(parentPath)){
                        componentPath = componentPath.startsWith("/")?componentPath.substr(1):componentPath;
                    }else{
                        componentPath = (parentPath.startsWith("/")?parentPath.substr(1):parentPath)+componentPath;
                    }*/
                }else{
                    componentPath = componentPath.startsWith("/")?componentPath.substr(1):componentPath;
                }
            }
            let item = {
                path:el.path,
                name:el.name,
                meta:{title:el.title,icon:el.icon||''},
                menu:el.menu,
                component:() => import(/* webpackChunkName: "[request]" */ '@/views'+components+'/'+componentPath)
            };
            if (el.children.length > 0){
                let child = hasAccess(el.children,parentPath?(parentPath+el.path):el.path);
                if (child.length > 0){
                    item.children = child;
                }
                //newRouterList = newRouterList.concat(hasAccess(el.children));
            }else {
                //router.component = () => import(/* webpackChunkName: "[request]" */ '@/views'+components+'/'+el.path.substr(1)+'');
                /*newRouterList.push({
                    path:el.path,
                    name:el.name,
                    meta:{title:el.title,icon:el.icon||''},
                    component:() => import(/!* webpackChunkName: "[request]" *!/ '@/views'+components+'/'+(el.path.startsWith("/")?el.path.substr(1):el.path)+'')
                });*/
            }
            newRouterList.push(item);
        }
    });
    return newRouterList;
}
function handlerChild(level,children,parentPath){
    let routes = [];
    if (!children){return routes;}
    for (let i = 0,len = children.length; i < len; i++) {
        let x = {...children[i]};
        /*if (parentPath && x.path){
            x.path = x.path.includes(parentPath)?x.path:(parentPath + x.path);
        }*/
        (parentPath) && (x.path = parentPath + x.path);
        if (x.children){
            let child = handlerChild(level+1,x.children,x.path);
            if (child.length > 0){
                routes = routes.concat(child);
            }else {
                if (x.path && x.name && x.component){
                    x.children && delete x.children;
                    routes.push(x);
                    continue;
                }
            }
        }
        if (level>=0 && x.path && x.name && x.component){
            x.children && delete x.children;
            routes.push(x);
        }
    }
    return routes;
}
const actions = {
    setToggleDevice({commit},device){
        commit(types.TOGGLE_DEVICE,device);
    },
	setAuthToken ({commit},isAuthToken) {
		commit(types.SET_TOKEN,isAuthToken);
	},
	clearLoginOut ({commit}) {
		commit(types.SET_USER,null);
		commit(types.SET_SIDERLIST,null);
		commit(types.SET_NEWROUTES,route);
		commit(types.SET_TOKEN,false);
	},
	setSiderList ({commit},siderList) {
		commit(types.SET_SIDERLIST,siderList);
	},
	concatRlues ({commit},userInfo) {
		return new Promise((resolve,reject) => {
			try {
                // true开启权限验证模式 ，false 不使用权限验证
                let key = route.findIndex(item => item.path==="/");
				let routerList = [],newRoute = [];
				if (userInfo.isAuth && userInfo.menus && Array.isArray(userInfo.menus) && userInfo.menus.length > 0){
					routerList = userInfo.menus;
					newRoute = hasAccess(routerList);
                    route[key].children = route[key].children.concat([...handlerChild(0,newRoute),routesMap.pop()]);
                }else{
                    route[key].children.push(handleRoutesMap.pop());
                }
                //newRoutes[key].children = userInfo.isAuth==false ? [...child,handleRoutesMap.pop()] : [...child,...hasAccess(routerList),routesMap.pop()];
                commit(types.SET_NEWROUTES,route);
				commit(types.SET_SIDERLIST,[...routes.filter(item => item.menu===true).concat(newRoute)]);
				commit(types.SET_USER,userInfo);
                resolve(route);
			 } catch(error) {
				console.log('%c ! filter router error ','background:#000;color:#bada55',error);
			 	reject(error);
			 }
		});
	}
};
export default actions;