import types from './types.js'

 // 后台直接请求回来菜单，帅选左侧导航菜单需要的属性
const getMenuList = (routesList,parentPath) => {
    let siderList = [];
    for (let i = 0,len = routesList.length; i < len; i++) {
        let item = routesList[i];
        if (item.menu === false || item.hidden === true || (!item.name && !item.children)) {
            continue;
        }
        if (item.path || item.children){
            let sider = {name:'', icon:'', path:''};
            sider.path = item.path;
            if (parentPath){
                //sider.path = item.path = item.path.includes(parentPath)?item.path:(parentPath+item.path);
                sider.path = item.path = parentPath+item.path;
            }
            if (item.meta && item.meta.title){
                sider.name = item.meta.title;
                sider.icon = item.meta.icon;
            }
            if (item.title){
                sider.name = item.title;
                sider.icon = item.icon || '';
            }
            if (item.children && item.children.length > 0){
                let child = getMenuList(item.children,item.path);
                if (child.length > 0){
                    sider.children = child;
                }
            }
            siderList.push(sider);
        }
        //sider.name && sider.path && siderList.push(sider);
    }
    return siderList;
};

const mutations = {
	[types.TOGGLE_DEVICE] (state,device) {
        state.device = device;
	},
    [types.SET_TOKEN] (state,isAuthToken) {
		state.isAuthToken = isAuthToken;
	},
	[types.SET_USER] (state,userInfo) {
		if (userInfo){
			state.userInfo = userInfo;
		}else{
			state.userInfo = {};
		}
	},
	[types.SET_NEWROUTES] (state,newRoutes) {
		state.routes = newRoutes;
		state.isAuthToken = true;
	},
	[types.SET_SIDERLIST] (state,siderList) {
		if(!siderList){
			return state.siderList = siderList;
		}
        siderList = getMenuList(siderList);
        if (Array.isArray(siderList) && siderList.length > 0) {
			state.siderList = siderList;
		}else {
			state.siderList = [];
		}
	}
};

export default mutations
