import Layout from '@/components/layout'
import {routes} from "../routes.js"

function handlerPath(routes) {
    let newRoutePath = [];
    if (!routes){
        return;
    }
    routes.forEach(x => {
        if (x.children && Array.isArray(x.children)){
            x.children = handlerPath(x.children);
        }
        // if (x.path.startsWith("/")){
        //     //x.path = x.path.replace("/","");
        // }
        newRoutePath.push(x);
    });
    return newRoutePath;
}
let home;
let login;
let newRoutes = [];
for (let i = 0,len = routes.length; i < len; i++) {
    let x = routes[i];
    if (x.login === true){
        login = x;
        continue;
    }
    if (x.path === "/"){
        home = x;
        continue;
    }
    if (x.path && !x.component && x.children){
        newRoutes.push(...handlerChild(x.children,x.path));
        continue;
    }
    newRoutes.push(x);
}
function handlerChild(children,parentPath){
    let routes = [];
    if (!children){return routes;}
    children.forEach(i => {
        let x = {...i};
        x.path = parentPath + x.path;
        if (x.children){
            routes = routes.concat(handlerChild(x.children,x.path));
        }
        if (x.path && x.name && x.component){
            routes.push(x);
        }
    });
    return routes;
}
let route = [];
if (login){
    route.push({...login});
}
if (home){
    let item = {
        ...home,
        component: Layout
    };
    if (item.children && Array.isArray(item.children)){
        item.children = handlerPath(item.children.concat(newRoutes));
    }
    route.push(item);
}
export default route;